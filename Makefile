all: superserver.exe tcpServer.exe tcpClient.exe udpServer.exe udpClient.exe

superserver.exe: superserver.o

	gcc -o superserver.exe superserver.o

superserver.o: superserver.c

	gcc -c superserver.c

tcpServer.exe: tcpServer.o

	gcc -o tcpServer.exe tcpServer.o

tcpServer.o: tcpServer.c

	gcc -c tcpServer.c
	
tcpClient.exe: tcpClient.o

	gcc -o tcpClient.exe tcpClient.o

tcpClient.o: tcpClient.c

	gcc -c tcpClient.c
	
udpServer.exe: udpServer.o

	gcc -o udpServer.exe udpServer.o

udpServer.o: udpServer.c

	gcc -c udpServer.c
	
udpClient.exe: udpClient.o

	gcc -o udpClient.exe udpClient.o

udpClient.o: udpClient.c

	gcc -c udpClient.c

.PHONY:	clean

clean:
	-rm -f *.exe *.o
